/**
 * AngularJS
 * @author Wahyu Sanjaya <wahyu.sanjaya@emeriocorp.com>
 */

var mainApp = angular.module('mainApp', [
  'ngRoute'

]);


mainApp.config(function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "templates/page1.html", controller: ""})
    // Pages
    .when("/page2", {templateUrl: "templates/page2.html", controller: ""})
    // else 404
    .otherwise("/404", {templateUrl: "templates/404.html", controller: ""});
});

mainApp.controller("globalController", function($scope, $http){
  $scope.message = "In global Controller";
  $scope.type = "Global";

  $scope.listData = {
    subjects: [{
      name: 'Physics',
      marks: 70
    },
    {
      name: 'Chemistry',
      marks: 80
    },
    {
      name: 'Math',
      marks: 65
    }
  ]
}

$http({
  method: 'GET',
  url: 'http://52.76.85.10/test/location.json'
}).then(function successCallback(response){
  console.log("successfully");
  $scope.listData = response;
}, function errorCallback(error){
  console.log(error);
});
});


mainApp.controller("firstController", function($scope){
  $scope.message = "In first Controller";
});

mainApp.controller("secondController", function($scope){
  $scope.message = "In second Controller";
  $scope.type = "Second";
});

mainApp.factory('MathService', function(){
  var factory = {};
  factory.multiply = function(a, b){
    return a * b
  }
  factory.add = function(a, b){
    return a + b
  }
  factory.subs = function(a, b){
    return a - b
  }
  factory.div = function(a, b){
    return a / b
  }
  return factory;
});

mainApp.service('CalcService', function(MathService){
  this.square = function(a,b){
      return MathService.multiply(a,b);
  }
  this.adds = function(a,b){
      return MathService.add(a,b);
  }
  this.subss = function(a,b){
      return MathService.subs(a,b);
  }
  this.divs = function(a,b){
      return MathService.div(a,b);
  }
});

mainApp.controller('CalcController', function($scope, CalcService){
  $scope.square = function(){
    $scope.result = CalcService.square($scope.number,$scope.number1);
  }
  $scope.adds = function(){
    $scope.result = CalcService.adds($scope.number,$scope.number1);
  }
  $scope.subss = function(){
    $scope.result = CalcService.subss($scope.number,$scope.number1);
  }
  $scope.divs = function(){
    $scope.result = CalcService.divs($scope.number,$scope.number1);
  }
});
